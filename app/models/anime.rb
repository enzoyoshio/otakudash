class Anime < ApplicationRecord
    has_many :seasons
    validates :name, presence: true
    validates :sinopse, presence: true
end
