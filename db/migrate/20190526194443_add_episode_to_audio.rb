class AddEpisodeToAudio < ActiveRecord::Migration[5.2]
  def change
    add_reference :audios, :episode, foreign_key: true
  end
end
