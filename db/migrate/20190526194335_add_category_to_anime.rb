class AddCategoryToAnime < ActiveRecord::Migration[5.2]
  def change
    add_reference :animes, :category, foreign_key: true
  end
end
