class AddAnimeToCategory < ActiveRecord::Migration[5.2]
  def change
    add_reference :categories, :anime, foreign_key: true
  end
end
