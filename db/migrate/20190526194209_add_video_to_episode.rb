class AddVideoToEpisode < ActiveRecord::Migration[5.2]
  def change
    add_column :episodes, :video, :BLOB
  end
end
