class AddAudioToEpisode < ActiveRecord::Migration[5.2]
  def change
    add_reference :episodes, :audio, foreign_key: true
  end
end
