# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_05_26_194443) do

  create_table "animes", force: :cascade do |t|
    t.string "name"
    t.text "sinopse"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "category_id"
    t.index ["category_id"], name: "index_animes_on_category_id"
  end

  create_table "audios", force: :cascade do |t|
    t.text "language"
    t.integer "subtitle_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "episode_id"
    t.index ["episode_id"], name: "index_audios_on_episode_id"
    t.index ["subtitle_id"], name: "index_audios_on_subtitle_id"
  end

  create_table "categories", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "anime_id"
    t.index ["anime_id"], name: "index_categories_on_anime_id"
  end

  create_table "episodes", force: :cascade do |t|
    t.integer "number"
    t.string "name"
    t.integer "duration"
    t.integer "Season_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.binary "video"
    t.integer "audio_id"
    t.index ["Season_id"], name: "index_episodes_on_Season_id"
    t.index ["audio_id"], name: "index_episodes_on_audio_id"
  end

  create_table "seasons", force: :cascade do |t|
    t.string "name"
    t.date "year"
    t.integer "anime_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "number"
    t.index ["anime_id"], name: "index_seasons_on_anime_id"
  end

  create_table "subtitles", force: :cascade do |t|
    t.text "text"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

end
